export const scrollToDown = (target) => {
  let block = document.getElementById(target);
  if(block) block.scrollTop = block.scrollHeight;
}