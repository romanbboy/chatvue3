export const debounce = ms => f => {
  let timeoutId;

  return function () {
    clearTimeout(timeoutId);
    timeoutId = setTimeout(() => {
      f.apply(this, arguments);
    }, ms);
  };
};