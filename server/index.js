const express = require('express');
const app = require('express')();
const server = require('http').createServer(app);
const io = require('socket.io')(server, {
  cors: {methods: ["GET", "POST"]}
});
const path = require('path');
const bodyParser = require('body-parser');
const cors = require('cors');
const morgan = require('morgan');

const chatRouter = require('./routes/chat');

const PORT = process.env.PORT || 7777;

app.use(morgan('combined'));
app.use(bodyParser.json());
app.use(cors({
  origin: true
}));

app.use((req,res,next) => {
  req.io = io;
  next();
});

app.use('/api/chat', chatRouter);
app.use(express.static(path.join(__dirname)));

io.on('connection', client => {
  client.on('join', () => {
    client.broadcast.emit('connectNewUser', 'К нам пришел новый чувак');
    client.emit('connectNewUser', 'Добро пожаловать')
  });

  client.on('typing', user => {
    client.broadcast.emit('noticeTyping', `${user} че то печатает...`);
  });

  client.on('clearType', () => {
    client.broadcast.emit('noticeClearType');
  });
});

server.listen(PORT, () => {
  console.log('-----> ', `Server start on port ${PORT}`);
});