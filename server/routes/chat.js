const {Router} = require('express');
const path = require('path');
const fs = require('fs')
const dbChat = require('../db/chat.json');

const router = Router();

router.get('/', (req, res) => {
  const json  = JSON.parse(fs.readFileSync(path.resolve(__dirname, '../db', 'chat.json'), 'utf-8'));

  res.send({
    data: json.chat
  })
});

router.post('/', (req, res) => {
  let json = {};
  json.chat = JSON.parse(fs.readFileSync(path.resolve(__dirname, '../db', 'chat.json'), 'utf-8')).chat;
  json.chat.push(req.body)

  fs.writeFileSync(path.resolve(__dirname, '../db', 'chat.json'), JSON.stringify(json), 'utf-8');
  req.io.emit('newMsg');

  res.send({
    success: true
  })
});

router.delete('/', (req, res) => {
  fs.writeFileSync(path.resolve(__dirname, '../db', 'chat.json'), JSON.stringify({chat: []}), 'utf-8');
  res.send({})
});

module.exports = router